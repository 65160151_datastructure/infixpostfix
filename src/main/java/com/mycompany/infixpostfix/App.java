/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.infixpostfix;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class App {
    public static void main(String[] args) throws IOException {
        Scanner kb = new Scanner(System.in);
        String input, output;
        while (true) {
            System.out.print("Enter infix: ");
            System.out.flush();
            input = kb.next();
            if (input.equals("")) {
                break;
            }
            Intopost theTrans = new Intopost(input);
            output = theTrans.doTrans();
            System.out.println("Postfix is " + output + '\n');
        }
    }
}
